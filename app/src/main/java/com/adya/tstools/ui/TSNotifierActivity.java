package com.adya.tstools.ui;

import com.adya.tstools.navigation.TSActivity;

public abstract class TSNotifierActivity extends TSActivity {

    @Override
    protected void onResume() {
        super.onResume();
        TSNotifier.getNotifier().initNotifier(this);
    }
}

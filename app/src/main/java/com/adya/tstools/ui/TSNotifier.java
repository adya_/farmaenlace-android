package com.adya.tstools.ui;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import com.kaopiz.kprogresshud.KProgressHUD;

/**
 * TSNotifier prototype. (from iOS)
 * */
public class TSNotifier {

    private static TSNotifier notifier;
    private TSNotifier(){}
    public static TSNotifier getNotifier() {
        if (notifier == null) notifier = new TSNotifier();
        return notifier;
    }

    private KProgressHUD hud;

    public void initNotifier(Context context){
        if (hud != null && hud.isShowing()) hud.dismiss();
        hud = KProgressHUD.create(context);
        hud.setCustomView(buildProgressView(context));
    }

    public void showProgress(){
        if (hud == null) Log.d(TSNotifier.class.getSimpleName(), "Hud wasn't initialized.");
        hud.setDimAmount(0.8f).show();
    }

    private View buildProgressView(Context context){
        ProgressBar pb = new ProgressBar(context);
        pb.setIndeterminate(true);
        return pb;
    }

    public void hideProgress(){
        if (hud != null) hud.dismiss();
    }
}

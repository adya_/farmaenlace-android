package com.adya.tstools.networking;

/**
 * TSResponse is a generic wrapper for responses.
 * */
public interface TSResponse<ResponseType> {
    boolean hasBody();
    ResponseType getBody();
}

package com.adya.tstools.networking;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Reborn TSRequestManager based on powerful Retrofit framework.
 */
public abstract class TSRetrofitManager {

    protected final <InnerType, ResponseType extends TSResponse<InnerType>> void request(Call<ResponseType> call, final TSResponseCallback<InnerType> callback) {
        if (call == null) return;
        call.enqueue(new Callback<ResponseType>() {
            @Override
            public void onResponse(Call<ResponseType> call, Response<ResponseType> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body());
                } else
                    callback.onFailure();
            }

            @Override
            public void onFailure(Call<ResponseType> call, Throwable t) {
                callback.onFailure();
            }
        });
    }

    public interface TSResponseCallback<T>{
        void onSuccess(TSResponse<T> response);
        void onFailure();
    }
}

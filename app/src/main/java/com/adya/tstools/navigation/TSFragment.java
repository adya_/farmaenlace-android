package com.adya.tstools.navigation;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by AdYa for simplifying the fragment management on Android platforms.
 * Ready-to-use fragment with automated typical actions.
 * Used only with TSActivity.
 * <p/>
 * Current version: 2.2.0
 * Release date: 21.10.2014
 * Copyright (C) 2014 Fragment Navigation Tools.
 */
public abstract class TSFragment extends Fragment {

    private View rootView;
    private StateSaver stateSaver = null;

    @Override
    public final View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(getLayoutResourceID(), container, false);
    }

    @Override
    public final void onViewCreated(View rootView, Bundle savedInstanceState) {
        super.onViewCreated(rootView, savedInstanceState);
        this.rootView = rootView;
        initializeFragmentView(rootView, savedInstanceState);
        if (stateSaver != null) {
            SharedPreferences prefs = getTSActivity().getSharedPreferences(getPrefsName(), Context.MODE_PRIVATE);
            stateSaver.restoreState(prefs);
            prefs.edit().clear().apply();
        }
    }

    /** Returns fragment's view. */
    protected View getRootView() {
        return rootView;
    }

    /**
     * Override this method to specify a way to refresh a view.
     */
    protected void updateView() {}

    /**
     * Returns parent activity.
     */
    public final TSActivity getTSActivity() {
        return (TSActivity) getActivity();
    }

    /**
     * Returns name of the preferences file.
     */
    private String getPrefsName() {
        return getClass().getName() + "_" + getTSActivity().getContainerID() + "_state";
    }

    /**
     * Checks whether activity, to which fragment is being attached, is instance of TSActivity class. It is necessary condition for correct work.
     */
    public void onAttach(Context context){
        if (!(context instanceof TSActivity))
            throw new IllegalArgumentException("TSFragment must be attached to a TSActivity.");
        super.onAttach(context);
        if (getTitle() != null && getFragmentManager().equals(getTSActivity().getSupportFragmentManager())) { // if it is the main fragment
            getTSActivity().setTitle(getTitle());
        }
    }

    /**
     * Takes care about "unlocking" UI elements when fragment is resuming.
     */
    @Override
    public void onResume() {
        super.onResume();
    }

    /**
     * Saves fragment's state when it is about to pause.
     */
    @Override
    public void onPause() {
        if (stateSaver != null) {
            SharedPreferences.Editor editor = getTSActivity().getSharedPreferences(getPrefsName(), Context.MODE_PRIVATE).edit();
            stateSaver.saveState(editor);
            editor.apply();
        }
        super.onPause();
    }

    /**
     * Set up stateSaver to save and restore fragments state.
     */
    protected final void setStateSaver(StateSaver stateSaver) {
        this.stateSaver = stateSaver;
    }

    /**
     * Returns fragment's title which can be displayed by the TSActivity.
     * If no title needed return null.
     */
    public String getTitle() {
        return null;
    }

    /**
     * Calls when fragment initializing its' view.
     * Override one of the following methods.
     */
    protected abstract void initializeFragmentView(View rootView, Bundle savedInstanceState);

    /**
     * Should return layout resource id, which will be inflated for the fragment.
     */
    protected abstract @LayoutRes int getLayoutResourceID();
}

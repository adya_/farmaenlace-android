package com.adya.tstools.navigation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * Created by AdYa for simplifying the fragment management on Android platforms.
 * Ready-to-use activity, which has automated and easy fragment manipulation management.
 * Designed to manipulate with TSFragments.
 * <p/>
 * Current version: 2.2.0
 * Release date: 21.10.2014
 * Copyright (C) 2014 Fragment Navigation Tools.
 */
public abstract class TSActivity extends AppCompatActivity {

    ///TODO: Clean up in TSActivity.
    // Need to move out some stuff from the TSActivity to TSUtilities.
    // Group methods and fields.


    private static final String PREFERENCE_SUFFIX = "_TSActivity_state";
    private static final String SHOULD_DISPLAY_FRAGMENT_TITLE_TAG = "display_fragment_title";
    private static final String ALLOW_EMPTY_TITLES_TAG = "allow_empty_titles";

    /**
     * If true fragments will override activity's title when displayed.
     */
    private boolean shouldDisplayFragmentsTitle = false;
    /**
     * If true titles can be set to empty, so nothing will be shown in title. Otherwise empty title will be replaced with app name.
     */
    private boolean allowEmptyTitles = false;


    public interface OnFragmentCloseListener {
        void onFragmentClose(Fragment fragment);
    }

    private OnFragmentCloseListener listiner;

    /**
     * Used to save and restore base TSActivity fields.
     */
    private StateSaver internalStateSaver = new StateSaver() {
        @Override
        public void saveState(final SharedPreferences.Editor editor) {
            editor.putBoolean(SHOULD_DISPLAY_FRAGMENT_TITLE_TAG, shouldDisplayFragmentsTitle);
            editor.putBoolean(ALLOW_EMPTY_TITLES_TAG, allowEmptyTitles);
        }

        @Override
        public void restoreState(final SharedPreferences preferences) {
            shouldDisplayFragmentsTitle = preferences.getBoolean(SHOULD_DISPLAY_FRAGMENT_TITLE_TAG, shouldDisplayFragmentsTitle);
            allowEmptyTitles = preferences.getBoolean(ALLOW_EMPTY_TITLES_TAG, allowEmptyTitles);
        }
    };

    private StateSaver stateSaver = null;

    /**
     * Set up stateSaver to save and restore fields of inherited activity.
     */
    protected void setStateSaver(StateSaver stateSaver) {
        this.stateSaver = stateSaver;
    }

    public boolean isShouldDisplayFragmentsTitle() {
        return shouldDisplayFragmentsTitle;
    }

    public void setShouldDisplayFragmentsTitle(boolean shouldDisplayFragmentsTitle) {
        this.shouldDisplayFragmentsTitle = shouldDisplayFragmentsTitle;
    }

    public boolean isAllowEmptyTitles() {
        return allowEmptyTitles;
    }

    public void setAllowEmptyTitles(boolean allowEmptyTitles) {
        this.allowEmptyTitles = allowEmptyTitles;
    }

    public String getApplicationName() {
        int stringId = getApplicationInfo().labelRes;
        return getString(stringId);
    }

    @Override
    public final void setTitle(@StringRes int resID) {
        setTitle(getString(resID));
    }

    @Override
    public final void setTitle(CharSequence title) {
        if ((isShouldDisplayFragmentsTitle() && title != null) &&
                (isAllowEmptyTitles() || title.length() != 0)) {
            displayTitle(title);
        } else displayTitle(getApplicationName());
    }

    /**
     * Override this method to provide custom displaying for title.
     */
    protected void displayTitle(CharSequence title) {
        super.setTitle(title);
    }

    /**
     * Should return resource id of the element which will contain fragments (or other views).
     */
    protected abstract @IdRes int getContainerID();

    /**
     * Should return layout resource id, which will be inflated for the activity.
     */
    protected abstract @LayoutRes int getLayoutResourceID();

    /**
     * Calls after activity created and restored its' state.
     */
    protected abstract void onTSActivityCreated(View activityView);

    public final View getRootView() {
        return findViewById(android.R.id.content);
    }

    public final View getContainerView() {
        return findViewById(getContainerID());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResourceID());
        SharedPreferences prefs = getSharedPreferences(getApplicationName() + PREFERENCE_SUFFIX, MODE_PRIVATE);
        internalStateSaver.restoreState(prefs);
        if (stateSaver != null) stateSaver.restoreState(prefs);
        prefs.edit().clear().apply();
        if (getInitialFragmentClass() != null) {
            replaceFragment(getInitialFragmentClass(), new TSFragmentNavigationConfiguration().setAddToBackStack(false));
        }
        onTSActivityCreated(getRootView());
    }

    @Override
    protected void onPause() {
        SharedPreferences.Editor editor = getSharedPreferences(getApplicationName() + PREFERENCE_SUFFIX, MODE_PRIVATE).edit();
        internalStateSaver.saveState(editor);
        if (stateSaver != null) stateSaver.saveState(editor);
        editor.apply();
        super.onPause();
    }

    public final void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        if (imm != null && getCurrentFocus() != null) {
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public void onBackPressed() {
        updateTitleFromFragment(getSupportFragmentManager());
        super.onBackPressed();
    }

    protected void updateTitleFromFragment(final FragmentManager fragmentManager){
        if (fragmentManager != null) {
            fragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
                @Override
                public void onBackStackChanged() {
                    if (fragmentManager.getFragments().size() > 0) {
                        TSFragment TSFragment = (TSFragment) fragmentManager.getFragments().get(0);
                        if (TSFragment != null && TSFragment.isAdded()) {
                            setTitle(TSFragment.getTitle());
                        }
                    }
                    fragmentManager.removeOnBackStackChangedListener(this);
                }
            });
        }
    }

    /**
     * Sets root view and all its' child views' touch listeners to hide keyboard when touched.
     *
     * @param view Root view.
     */
    protected final void setupSoftKeyboardHide(View view) {

        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideKeyboard();
                    return false;
                }
            });
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupSoftKeyboardHide(innerView);
            }
        }
    }


    /**
     * Returns class of the fragment which will be displayed on activity startup.
     */
    public abstract <T extends Fragment> Class<T> getInitialFragmentClass();

    /**
     * Clears back stack in default fragment manager (getSupportFragmentManager()).
     */
    public void clearBackStack() {
        clearBackStack(getSupportFragmentManager());
    }

    /**
     * Clears back stack in specific fragment manager.
     *
     * @param manager Fragment manager where back stack should be cleared.
     */
    public void clearBackStack(FragmentManager manager) {
        manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    /**
     * Set up listener for closing fragments event.
     */
    public final void setOnFragmentCloseListener(OnFragmentCloseListener listener) {
        this.listiner = listener;
    }

    /**
     * Closes current fragment in default fragment manager (getSupportedFragmentManager()).
     */
    public void closeCurrentFragment() {
        closeFragment(getContainerID(), getSupportFragmentManager());
    }

    /**
     * Closes specified fragment in default fragment manager (getSupportedFragmentManager()).
     *
     * @param cls Class of the fragment to be closed.
     */
    public final <T extends Fragment> void closeFragment(Class<T> cls) {
        closeFragment(cls, getSupportFragmentManager());
    }

    /**
     * Closes current fragment in given container with appropriate fragment manager.
     *
     * @param containerID     ID of the container where the fragment is.
     * @param fragmentManager Fragment manager which manages given container.
     */
    public final void closeFragment(@IdRes final int containerID, final FragmentManager fragmentManager) {
        final Fragment fragment = fragmentManager.findFragmentById(containerID);
        closeFragment(fragment, fragmentManager);
    }

    /**
     * Closes specified fragment if it is currently displayed with appropriate fragment manager.
     *
     * @param cls             Class of the fragment to be closed.
     * @param fragmentManager Fragment manager which manages given container
     */
    public final <T extends Fragment> void closeFragment(Class<T> cls, final FragmentManager fragmentManager) {
        final Fragment fragment = fragmentManager.findFragmentByTag(cls.getName());
        closeFragment(fragment, fragmentManager);
    }

    private void closeFragment(final Fragment fragment, final FragmentManager fragmentManager) {
        if (fragmentManager != null && fragment != null) {
            updateTitleFromFragment(fragmentManager);
            fragmentManager.beginTransaction().remove(fragment).commit();
            fragmentManager.popBackStack(fragment.getClass().getName(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }


    public final TSFragmentNavigationConfiguration createNavigationConfiguration(){
        return new TSFragmentNavigationConfiguration();
    }

    /**
     * Replaces fragment in managed container using specified fragment manager.
     * Note: Be aware! When using multiple FragmentManagers be sure which fragments attached to each manager, otherwise you probably will have regular crashes.
     *
     * @param cls            Target Fragment class.
     * @param configuration  Configuration for this transaction.
     * @param <T>            Fragment Type which extends base Fragment.
     * @return instance of target fragment displayed on the screen.
     */
    public final <T extends Fragment> T replaceFragment(Class<T> cls, TSFragmentNavigationConfiguration configuration) {
        FragmentManager manager = configuration.getManager();
        @IdRes int containerID = configuration.getContainerID();
        boolean addToBackStack = configuration.shouldAddToBackStack();
        Bundle bundle = configuration.getBundle();
        T fragment = (T)manager.findFragmentByTag(cls.getName()); ///TODO: Add an option to reuse fragment or recreate it regardless.
        if (fragment == null) {
            fragment = (T) T.instantiate(this, cls.getName(), null);
        }
        if (fragment.isVisible()) {
            return fragment;
        }
        fragment.setArguments(bundle);
        FragmentTransaction ft = manager.beginTransaction();
        if (addToBackStack) ft.addToBackStack(cls.getName());
        if (manager.equals(getSupportFragmentManager()))
            updateTitleFromFragment(manager);
        ft.replace(containerID, fragment, cls.getName()).commitAllowingStateLoss();

        return fragment;
    }

    /**
     * Replaces fragment in managed container using default fragment manager (getSupportFragmentManager()).
     * Adds fragment to back stack by default.
     *
     * @param cls Target Fragment class.
     * @param <T> Fragment Type which extends base Fragment.
     * @return instance of target fragment displayed on the screen.
     */
    public final <T extends Fragment> T replaceFragment(Class<T> cls) {
        return replaceFragment(cls, createNavigationConfiguration());
    }

    /**
     * Replaces fragment in managed container using specified fragment manager.
     * Note: Be aware! When using multiple FragmentManagers be sure which fragments attached to each manager, otherwise you probably will have regular crashes.
     *
     * @param cls            Target Fragment class.
     * @param configuration  Configuration for this transaction.
     * @param <T>            Fragment Type which extends base Fragment.
     * @return instance of target fragment displayed on the screen.
     */
    public final <T extends Fragment> T addFragment(Class<T> cls, TSFragmentNavigationConfiguration configuration) {
        FragmentManager manager = configuration.getManager();
        @IdRes int containerID = configuration.getContainerID();
        boolean addToBackStack = configuration.shouldAddToBackStack();
        Bundle bundle = configuration.getBundle();

        T fragment = (T) manager.findFragmentByTag(cls.getName()); ///TODO: Add an option to reuse fragment or recreate it regardless.
        if (fragment == null) {
            fragment = (T) T.instantiate(this, cls.getName(), null);
        }
        if (fragment.isVisible()) {
            return fragment;
        }
        fragment.setArguments(bundle);
        FragmentTransaction ft = manager.beginTransaction();
        if (addToBackStack) ft.addToBackStack(cls.getName());
        Fragment f = manager.findFragmentById(containerID);
        if (f != null) {
            ft.hide(f);
            if (listiner != null && manager.equals(getSupportFragmentManager()))
                listiner.onFragmentClose(f);
        }
        if (fragment.isAdded() || fragment.isHidden()) {
            ft.show(fragment);
        } else {
            ft.add(containerID, fragment, cls.getName()).show(fragment);
        }
        if (manager.equals(getSupportFragmentManager()))
            updateTitleFromFragment(manager);
        ft.commitAllowingStateLoss();
        return fragment;
    }

    /**
     * Replaces fragment in managed container using specified fragment manager.
     * Note: Be aware! When using multiple FragmentManagers be sure which fragments attached to each manager, otherwise you probably will have regular crashes.
     *
     * @param cls            Target Fragment class.
     * @param <T>            Fragment Type which extends base Fragment.
     * @return instance of target fragment displayed on the screen.
     */
    public final <T extends Fragment> T addFragment(Class<T> cls){
        return addFragment(cls, createNavigationConfiguration());
    }

    /**
     * Starts specified activity with extra data.
     * Note: parameter intent is used for sending data. If there is no data intent can be null.
     *
     * @param activityClass Class of the activity to be started.
     * @param intent        Intent with extras.
     */
    public final <T extends Activity> void startActivity(Class<T> activityClass, Intent intent) {
        if (intent == null)
            intent = new Intent(this, activityClass);
        else
            intent.setClass(this, activityClass);
        super.startActivity(intent);
    }

    /**
     * Starts specified activity without extra data.
     *
     * @param activityClass Class of the activity to be started.
     */
    public final <T extends Activity> void startActivity(Class<T> activityClass) {
        startActivity(activityClass, null);
    }

    /**
     * Represents supported configuration for fragment transactions.
     * */
    public class TSFragmentNavigationConfiguration {
        private boolean addToBackStack;
        private FragmentManager manager;
        private @IdRes int containerID;
        private Bundle bundle;

        /**
         * Default configuration.
         * */
        public TSFragmentNavigationConfiguration() {
            addToBackStack = true;
            manager = TSActivity.this.getSupportFragmentManager();
            containerID = TSActivity.this.getContainerID();
            bundle = null;
        }

        public boolean shouldAddToBackStack() {
            return addToBackStack;
        }

        public TSFragmentNavigationConfiguration setAddToBackStack(boolean addToBackStack) {
            this.addToBackStack = addToBackStack;
            return this;
        }

        public FragmentManager getManager() {
            return manager;
        }

        public TSFragmentNavigationConfiguration setFragmentManager(FragmentManager manager) {
            this.manager = manager;
            return this;
        }

        public @IdRes int getContainerID() {
            return containerID;
        }

        public TSFragmentNavigationConfiguration setContainerID(@IdRes int containerID) {
            this.containerID = containerID;
            return this;
        }

        public Bundle getBundle() {
            return bundle;
        }

        public TSFragmentNavigationConfiguration setBundle(Bundle bundle) {
            this.bundle = bundle;
            return this;
        }
    }
}

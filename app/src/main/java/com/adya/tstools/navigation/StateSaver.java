package com.adya.tstools.navigation;

import android.content.SharedPreferences;

/**
 * Interface which provides methods to save/restore object state using given SharedPreferences.
 */
public interface StateSaver {
    void saveState(final SharedPreferences.Editor editor);

    void restoreState(final SharedPreferences preferences);
}

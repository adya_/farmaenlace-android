package com.xenio.adya.farmaenlace.ui.fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import com.xenio.adya.farmaenlace.R;
import com.adya.tstools.navigation.TSActivity;
import com.adya.tstools.navigation.TSFragment;

public class LocateFragment extends TSFragment {

    @Override
    protected void initializeFragmentView(View rootView, Bundle savedInstanceState) {
        Button bPublish = (Button) rootView.findViewById(R.id.bLocatePublish);
        bPublish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTSActivity().finish();
            }
        });

        LinearLayout llLink = (LinearLayout) rootView.findViewById(R.id.llLocateFindMapLink);
        llLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle=  new Bundle();
                bundle.putBoolean(LocateMapFragment.ARGUMENT_KEY_EDITABLE, true);
                TSActivity.TSFragmentNavigationConfiguration configuration = getTSActivity().createNavigationConfiguration().setBundle(bundle);
                getTSActivity().addFragment(LocateMapFragment.class, configuration);
            }
        });
    }

    @Override
    protected int getLayoutResourceID() {
        return R.layout.fragment_locate;
    }

    @Override
    public String getTitle() {
        return getString(R.string.text_header_locate);
    }
}

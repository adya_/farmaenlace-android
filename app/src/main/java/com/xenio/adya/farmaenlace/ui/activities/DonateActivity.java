package com.xenio.adya.farmaenlace.ui.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.xenio.adya.farmaenlace.ui.fragments.DonateFragment;

public class DonateActivity extends NavigationActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public <T extends Fragment> Class<T> getInitialFragmentClass() {
        return (Class<T>) DonateFragment.class;
    }
}

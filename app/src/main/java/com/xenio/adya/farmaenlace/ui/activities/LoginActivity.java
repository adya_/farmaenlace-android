package com.xenio.adya.farmaenlace.ui.activities;

import android.support.v4.app.Fragment;
import android.view.View;
import com.adya.tstools.ui.TSNotifierActivity;
import com.xenio.adya.farmaenlace.R;
import com.xenio.adya.farmaenlace.ui.fragments.LoginFragment;

public class LoginActivity extends TSNotifierActivity {

    @Override
    protected int getContainerID() {
        return android.R.id.content;
    }

    @Override
    protected int getLayoutResourceID() {
        return R.layout.activity_login;
    }

    @Override
    protected void onTSActivityCreated(View activityView) {}

    @Override
    public <T extends Fragment> Class<T> getInitialFragmentClass() {
        return (Class<T>) LoginFragment.class;
    }

}

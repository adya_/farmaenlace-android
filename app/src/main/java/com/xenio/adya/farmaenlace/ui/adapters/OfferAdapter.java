package com.xenio.adya.farmaenlace.ui.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import com.xenio.adya.farmaenlace.R;
import com.xenio.adya.farmaenlace.models.Offer;
import com.xenio.adya.farmaenlace.utils.OfferUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class OfferAdapter extends BaseAdapter{

    private ArrayList<Offer> offers;
    private Context context;
    private LayoutInflater inflater;

    public OfferAdapter(Context context, Collection<Offer> offers){
        this.offers = new ArrayList<>(offers);
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    public OfferAdapter(Context context, Offer[] offers){
        this.offers = new ArrayList<>(Arrays.asList(offers));
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return offers.size();
    }

    @Override
    public Offer getItem(int i) {
        return offers.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View recyledView, ViewGroup viewGroup) {
        View view = recyledView;
        if (view == null){
            view = inflater.inflate(R.layout.item_have, viewGroup, false);
        }
        TextView tvMedicines = (TextView) view.findViewById(R.id.tvHaveItemMedicine);
        TextView tvDetails = (TextView) view.findViewById(R.id.tvHaveItemDetails);
        final ImageButton ibRemove = (ImageButton) view.findViewById(R.id.ibHaveItemRemove);
        final Offer offer = getItem(i);

        tvMedicines.setText(TextUtils.join(", ", OfferUtils.getOfferedMedicinesNames(offer)));
        tvDetails.setText(TextUtils.join(", ", OfferUtils.getOfferedMedicinesDetails(offer)));

        ibRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                offers.remove(i);
                notifyDataSetChanged();
            }
        });
        return view;
    }

    public void add(Offer offer){
        offers.add(offer);
        notifyDataSetChanged();
    }

}

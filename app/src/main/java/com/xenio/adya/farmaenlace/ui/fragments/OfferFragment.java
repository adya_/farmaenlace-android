package com.xenio.adya.farmaenlace.ui.fragments;


import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.xenio.adya.farmaenlace.R;
import com.adya.tstools.navigation.TSActivity;
import com.adya.tstools.navigation.TSFragment;
import com.xenio.adya.farmaenlace.models.Offer;
import com.xenio.adya.farmaenlace.models.OfferType;
import com.xenio.adya.farmaenlace.utils.OfferUtils;

public class OfferFragment extends TSFragment {

    public static final String ARGUMENT_KEY_OFFER = "Offer";

    private Offer offer;

    @Override
    protected void initializeFragmentView(View rootView, Bundle savedInstanceState) {

        Bundle args = getArguments();
        offer = (Offer) args.getSerializable(ARGUMENT_KEY_OFFER);
        boolean location = (offer.getType() == OfferType.Locate);

        TextView tvTitle = (TextView) rootView.findViewById(R.id.tvOfferDetailsTitle);
        TextView tvShowMapLink = (TextView) rootView.findViewById(R.id.tvOfferDetailsShowMapLink);
        TextView tvName = (TextView) rootView.findViewById(R.id.tvOfferDetailsName);
        TextView tvMedicines = (TextView) rootView.findViewById(R.id.tvOfferDetailsMedicines);
        TextView tvType = (TextView) rootView.findViewById(R.id.tvOfferDetailsType);
        TextView tvAddress = (TextView) rootView.findViewById(R.id.tvOfferDetailsAddress);

        Button bProfile = (Button) rootView.findViewById(R.id.bOfferDetailsBack);
        Button bCall = (Button) rootView.findViewById(R.id.bOfferDetailsCall);

        tvTitle.setText(location ? R.string.text_title_offer_details_location : R.string.text_title_offer_details_contact);
        if (offer.getContact() != null) {
            tvName.setText(offer.getContact().getFullname());
            tvMedicines.setText(TextUtils.join(", ", OfferUtils.getOfferedMedicinesNames(offer)));
            bCall.setText(getString(R.string.pattern_button_offer_details_call, offer.getContact().getPhone()));
        }
        if (location && offer.getContact() != null)
            tvAddress.setText(offer.getContact().getAddress());
        else
            tvType.setText(offerName(offer.getType()));

        rootView.findViewById(R.id.llOfferDetailsShowMapLink).setVisibility(location ? View.VISIBLE : View.GONE);
        tvShowMapLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle args = new Bundle();
                args.putBoolean(LocateMapFragment.ARGUMENT_KEY_EDITABLE, false);
                args.putSerializable(LocateMapFragment.ARGUMENT_KEY_OFFER, offer);
                TSActivity.TSFragmentNavigationConfiguration configuration = getTSActivity().createNavigationConfiguration().setBundle(args);
                getTSActivity().addFragment(LocateMapFragment.class, configuration);
            }
        });
        rootView.findViewById(location ? R.id.llOfferDetailsType : R.id.llOfferDetailsAddress).setVisibility(View.GONE);  // hide extra field

        bCall.setVisibility(location? View.GONE : View.VISIBLE);

        bCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTSActivity().closeCurrentFragment();
            }
        });

        bProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTSActivity().closeCurrentFragment();
            }
        });

    }

    @Override
    protected int getLayoutResourceID() {
        return R.layout.fragment_offer_details;
    }

    @Override
    public String getTitle() {
        return getString(R.string.text_header_contact);
    }

    private String offerName(OfferType type){
        switch (type){
            case Exchange:return getString(R.string.value_offer_name_exchange);
            case Donate: return getString(R.string.value_offer_name_donate);
            case Locate: return getString(R.string.value_offer_name_locate);
            default: return "";
        }
    }
}

package com.xenio.adya.farmaenlace.ui.fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.adya.tstools.navigation.TSFragment;
import com.xenio.adya.farmaenlace.R;

public class ForgotFragment extends TSFragment {

    @Override
    protected void initializeFragmentView(View rootView, Bundle savedInstanceState) {
        Button bSend = (Button) rootView.findViewById(R.id.bSend);
        bSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTSActivity().closeCurrentFragment();
            }
        });
    }

    @Override
    protected int getLayoutResourceID() {
        return R.layout.fragment_forgot;
    }
}

package com.xenio.adya.farmaenlace.utils;

import android.text.TextUtils;
import com.xenio.adya.farmaenlace.models.Medicine;

public class MedicineUtils {

    private MedicineUtils(){}

    public static boolean containsMedicine(final String[] targetNames, final String name){
        if (targetNames != null && (targetNames.length > 0) && !TextUtils.isEmpty(name)) {
            String lowName = name.toLowerCase();
            for (String item : targetNames) {
                if (lowName.equals(item.toLowerCase())) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean containsMedicine(final Medicine[] targetMedicines, final String name){
        if (targetMedicines != null && (targetMedicines.length > 0)) {
            return containsMedicine(getMedicinesNames(targetMedicines), name);
        }
        return false;
    }

    public static boolean isUniqueMedicine(final String[] medicines, final String name){
        if (medicines != null && (medicines.length > 0) && !TextUtils.isEmpty(name)) {
            if (containsMedicine(medicines, name)){
                String lowName = name.toLowerCase();
                for (String item : medicines) {
                    String lowItem = item.toLowerCase();
                    if (lowItem.contains(lowName) && !lowItem.equals(lowName)) return false;   // if contains there are medicines which have search term in its name than it's not unique.
                }
                return true;
            }
        }
        return false;
    }

    public static boolean isUniqueMedicine(final Medicine[] medicines, final String name){
        if (medicines != null && (medicines.length > 0)){
            return isUniqueMedicine(getMedicinesNames(medicines), name);
        }
        return false;
    }

    public static String[] getMedicinesNames(final Medicine[] medicines){
        if (medicines == null || medicines.length == 0) return new String[0];
        String[] names = new String[medicines.length];
        for (int i = 0; i < names.length; i++) {
            names[i]= medicines[i].getName();
        }
        return names;
    }

    public static String[] getMedicinesDetails(final Medicine[] medicines){
        if (medicines == null || medicines.length == 0) return new String[0];
        String[] details = new String[medicines.length];
        for (int i = 0; i < details.length; i++) {
            details[i]= medicines[i].getDetails();
        }
        return details;
    }
}

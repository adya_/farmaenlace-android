package com.xenio.adya.farmaenlace.ui.adapters;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.xenio.adya.farmaenlace.R;
import com.xenio.adya.farmaenlace.models.ExchangeOffer;
import com.xenio.adya.farmaenlace.models.Offer;
import com.xenio.adya.farmaenlace.models.OfferType;
import com.xenio.adya.farmaenlace.utils.OfferUtils;

import java.util.ArrayList;

public class SearchAdapter extends BaseAdapter {

    private Offer[] offers;
    private Context context;
    private LayoutInflater inflater;

    public SearchAdapter(Context context, ArrayList<Offer> offers){
        this.offers = offers.toArray(new Offer[0]);
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    public SearchAdapter(Context context, Offer[] offers){
        this.offers = offers;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return offers.length;
    }

    @Override
    public Offer getItem(int i) {
        return offers[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View recyledView, ViewGroup viewGroup) {
        View view = recyledView;
        if (view == null){
            view = inflater.inflate(R.layout.item_search, viewGroup, false);
        }
        TextView tvOffer = (TextView) view.findViewById(R.id.tvSearchItemOfferType);
        TextView tvMedicines = (TextView) view.findViewById(R.id.tvSearchItemMedicine);
        TextView tvDetails = (TextView) view.findViewById(R.id.tvSearchItemDetails);
        TextView tvExchanged = (TextView) view.findViewById(R.id.tvSearchItemExchanged);
        ImageView ivIcon = (ImageView) view.findViewById(R.id.ivSearchItemIcon);

        Offer offer = getItem(i);

        ivIcon.setImageResource(offerIcon(offer.getType()));
        tvOffer.setText(offerName(offer.getType()));
        tvMedicines.setText(TextUtils.join(", ", OfferUtils.getOfferedMedicinesNames(offer)));
        tvDetails.setText(TextUtils.join(", ", OfferUtils.getOfferedMedicinesDetails(offer)));

        tvExchanged.setVisibility(offer.getType() == OfferType.Exchange ? View.VISIBLE : View.GONE);
        if (offer.getType() == OfferType.Exchange) {
            tvExchanged.setText(context.getString(R.string.pattern_label_history_item_looking, TextUtils.join(", ", OfferUtils.getRequestedMedicinesNames(((ExchangeOffer) offer)))));
        }
        return view;
    }

    private String offerName(OfferType type){
        switch (type){
            case Exchange:return context.getString(R.string.value_offer_name_exchange);
            case Donate: return context.getString(R.string.value_offer_name_donate);
            case Locate: return context.getString(R.string.value_offer_name_locate);
            default: return "";
        }
    }

    private @DrawableRes int offerIcon(OfferType type){
        switch (type) {
            case Exchange: return R.drawable.exchange_blue;
            case Donate:return R.drawable.donate_blue;
            case Locate:return R.drawable.locate_blue;
            default: return R.drawable.exchange_blue;
        }
    }
}

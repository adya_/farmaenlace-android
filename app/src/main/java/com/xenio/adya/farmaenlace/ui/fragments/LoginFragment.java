package com.xenio.adya.farmaenlace.ui.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.adya.tstools.navigation.TSFragment;
import com.adya.tstools.ui.TSNotifier;
import com.xenio.adya.farmaenlace.R;
import com.xenio.adya.farmaenlace.managers.UserManager;
import com.xenio.adya.farmaenlace.models.User;
import com.xenio.adya.farmaenlace.ui.activities.HomeActivity;

public class LoginFragment extends TSFragment {

    private Button bLogin;
    private EditText etLogin;
    private  EditText etPassword;

    private String login;
    private String password;

    @Override
    protected void initializeFragmentView(View rootView, Bundle savedInstanceState) {
        bLogin = (Button) rootView.findViewById(R.id.bLogin);
        Button bRegister = (Button) rootView.findViewById(R.id.bRegister);
        TextView tvForgot = (TextView) rootView.findViewById(R.id.tvForgotPassword);

        etLogin = (EditText) rootView.findViewById(R.id.etLogin);
        etPassword = (EditText) rootView.findViewById(R.id.etPassword);

        etLogin.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void afterTextChanged(Editable editable) {
                login = editable.toString();
                validateCredentials();
            }
        });
        etPassword.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void afterTextChanged(Editable editable) {
                password = editable.toString();
                validateCredentials();
            }
        });

        bLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TSNotifier.getNotifier().showProgress();
                UserManager.getManager().performLogin(login, password, new UserManager.LoginCallback() {
                    @Override
                    public void onLogin(User user) {
                        TSNotifier.getNotifier().hideProgress();
                        getTSActivity().finish();
                        getTSActivity().startActivity(HomeActivity.class);
                    }

                    @Override
                    public void onFailure(UserManager.LoginError error) {
                        TSNotifier.getNotifier().hideProgress();
                    }
                });
            }
        });

        bRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTSActivity().addFragment(RegisterFragment.class);
            }
        });

        tvForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTSActivity().addFragment(ForgotFragment.class);
            }
        });

        //TODO: actually this is for placeholder test fields
        login= etLogin.getText().toString();
        password = etPassword.getText().toString();

        validateCredentials();
    }

    @Override
    protected int getLayoutResourceID() {
        return R.layout.fragment_login;
    }

    private void validateCredentials(){
        boolean isValid = (!TextUtils.isEmpty(login) && !TextUtils.isEmpty(password) && (password.length() >= 4));
        bLogin.setEnabled(isValid);
    }
}

package com.xenio.adya.farmaenlace.ui.fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import com.xenio.adya.farmaenlace.R;
import com.adya.tstools.navigation.TSFragment;
import com.xenio.adya.farmaenlace.ui.activities.DonateActivity;
import com.xenio.adya.farmaenlace.ui.activities.ExchangeActivity;
import com.xenio.adya.farmaenlace.ui.activities.LocateActivity;

public class MainHomeFragment extends TSFragment {
    @Override
    protected void initializeFragmentView(View rootView, Bundle savedInstanceState) {
        ImageButton bExchange = (ImageButton) rootView.findViewById(R.id.bExchange);
        bExchange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTSActivity().startActivity(ExchangeActivity.class);
            }
        });

        ImageButton bDonate = (ImageButton) rootView.findViewById(R.id.bDonate);
        bDonate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTSActivity().startActivity(DonateActivity.class);
            }
        });

        ImageButton bLocate = (ImageButton) rootView.findViewById(R.id.bLocate);
        bLocate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTSActivity().startActivity(LocateActivity.class);
            }
        });
    }

    @Override
    protected int getLayoutResourceID() {
        return R.layout.fragment_home_main;
    }
}

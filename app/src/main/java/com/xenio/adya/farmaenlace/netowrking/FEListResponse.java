package com.xenio.adya.farmaenlace.netowrking;

import com.adya.tstools.networking.TSResponse;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FEListResponse<ResponseType> implements TSResponse<List<ResponseType>> {

    @SerializedName("ERROR_DESCRIPTION")
    private String message;

    @SerializedName("ERROR")
    private String errorCode;

    @SerializedName("SUCCESS")
    private boolean success;

    @SerializedName("LIST_ARRAY")
    private List<ResponseType> body;

    @Override
    public boolean hasBody() {
        return true;
    }

    @Override
    public List<ResponseType> getBody() {
        return body;
    }
}

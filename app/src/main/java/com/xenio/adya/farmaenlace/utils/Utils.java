package com.xenio.adya.farmaenlace.utils;

import com.google.android.gms.maps.model.LatLng;
import com.xenio.adya.farmaenlace.models.*;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Utils {

    private Utils() {}

    public static Offer[] generateTestOffers() {
        return new Offer[]{
                new DonationOffer(new Medicine[]{
                                        new Medicine("Test Med 1", "250 mg"),
                                        new Medicine("Test Med 2", "3 packs")},
                                  generateTestContact()),
                new LocationOffer(new Medicine[]{
                                        new Medicine("Test Med 3", "100 mg"),
                                        new Medicine("Test Med 4", "1 pack")},
                                  generateTestLocation(),
                                  generateTestContact()),
                new DonationOffer(new Medicine[]{
                                        new Medicine("Test Med 5", "350 mg"),
                                        new Medicine("Test Med 6", "1 packs"),
                                        new Medicine("Test Med 7", "100 mg")},
                                  generateTestContact()),
                new ExchangeOffer(new Medicine[]{
                                        new Medicine("Test Med 8", "350 mg"),
                                        new Medicine("Test Med 9", "3 packs")},
                                  new Medicine[]{
                                        new Medicine("Test Med 10", "150 mg"),
                                        new Medicine("Test Med 11", "2 packs")},
                                  generateTestContact()),
                new ExchangeOffer(new Medicine[]{
                                        new Medicine("Test Med 12", "350 mg"),
                                        new Medicine("Test Med 13", "3 packs")},
                                  new Medicine[]{
                                        new Medicine("Test Med 14", "150 mg"),
                                        new Medicine("Test Med 15", "2 packs")},
                                  generateTestContact()
                                  )
        };

    }

    public static Offer[] generateTestHistoryOffers() {
        Offer[] offers = generateTestOffers();
        offers[0].setCompletionDate(Calendar.getInstance().getTime());
        offers[2].setCompletionDate(new GregorianCalendar(2016, 3, 22).getTime());
        offers[2].setCompletionDate(new GregorianCalendar(2016, 1, 10).getTime());
        offers[3].setCompletionDate(new GregorianCalendar(2015, 2, 13).getTime());
        offers[4].setCompletionDate(new GregorianCalendar(2015, 4, 10).getTime());
        return offers;
    }

    public static Contact generateTestContact(){
        return new Contact("John", "Tester", "1111 111 111", "32 Some ave, apt. 11");
    }

    public static LatLng generateTestLocation(){
        return new LatLng(-34, 151);
    }
}

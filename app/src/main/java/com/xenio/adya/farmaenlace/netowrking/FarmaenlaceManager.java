package com.xenio.adya.farmaenlace.netowrking;

import com.adya.tstools.networking.TSRetrofitManager;
import com.google.gson.GsonBuilder;
import com.xenio.adya.farmaenlace.models.Medicine;
import com.xenio.adya.farmaenlace.models.Offer;
import com.xenio.adya.farmaenlace.models.User;
import com.xenio.adya.farmaenlace.netowrking.converters.OfferJSONDeserializer;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.List;

public class FarmaenlaceManager extends TSRetrofitManager {

    private FarmaenlaceService service;

    private static FarmaenlaceManager manager;

    private FarmaenlaceManager() {
      //  HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
      //  interceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
      //  OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(FarmaenlaceService.BaseURL)
                                                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                                                                                                .registerTypeAdapter(Offer.class, new OfferJSONDeserializer())
                                                                                                .create()))
                                                .build();
        service = retrofit.create(FarmaenlaceService.class);
    }

    public static FarmaenlaceManager getManager() {
        if (manager == null) manager = new FarmaenlaceManager();
        return manager;
    }

    public void login(String username, String password, TSResponseCallback<List<User>> callback){
        request(service.login(username, password), callback);
    }

    public void logout(String token, TSResponseCallback callback){
        request(service.logout(token), callback);
    }

    public void getMedicines(String medicineName, String token, TSResponseCallback<List<Offer>> callback){
        request(service.getMedicines(medicineName, token), callback);
    }

    public void getSimilarMedicines(String medicineName, String token, TSResponseCallback<List<Medicine>> callback){
        request(service.getSimilarMedicines(medicineName, token), callback);
    }

    public void addMedicineLocation(String medicineName, String locationName, String locationAddress, String locationCoords, String token, TSResponseCallback callback){
        request(service.addMedicineLocation(medicineName, locationName, locationAddress, locationCoords, token), callback);
    }

    public void removeMedicineLocation(int id, String token, TSResponseCallback callback){
        request(service.removeMedicineLocation(id, token), callback);
    }

    public void addMedicinesExchange(String medicineName, String medicineDetails, String token, TSResponseCallback callback){
        request(service.addMedicinesExchange(medicineName, medicineDetails, token), callback);
    }

    public void removeMedicinesExchange(int id, String token, TSResponseCallback callback){
        request(service.removeMedicinesExchange(id, token), callback);
    }

    public void addMedicinesDonation(String medicineName, String medicineDetails, String token, TSResponseCallback callback){
        request(service.addMedicinesDonation(medicineName, medicineDetails, token), callback);
    }

    public void removeMedicinesDonation(int id, String token, TSResponseCallback callback){
        request(service.removeMedicinesDonation(id, token), callback);
    }
}

package com.xenio.adya.farmaenlace.ui.activities;

import android.app.Activity;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import com.adya.tstools.ui.TSNotifierActivity;
import com.xenio.adya.farmaenlace.R;

public abstract class NavigationActivity extends TSNotifierActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar toolbar;
    private DrawerLayout drawer;

    @Override
    protected void displayTitle(CharSequence title) {
        TextView tv = (TextView) findViewById(R.id.tvHeader);
        tv.setText(title);
    }

    @Override
    protected int getContainerID() {
        return R.id.fragment_container;
    }

    @Override
    protected int getLayoutResourceID() {
        return R.layout.activity_navigation;
    }

    @Override
    protected void onTSActivityCreated(View activityView) {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        setShouldDisplayFragmentsTitle(true);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            if (!(this instanceof HomeActivity)) {
                finish(); // if any other activity is running - just finish it, there is always HomeActivity beneath.
            }
        } else if (id == R.id.nav_profile) {
            openActivity(ProfileActivity.class);
        } else if (id == R.id.nav_how_it_works) {

        } else if (id == R.id.nav_history){
            openActivity(HistoryActivity.class);
        } else if (id == R.id.nav_leave){
            finish();
            startActivity(LoginActivity.class);
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private <T extends Activity> void openActivity(Class<T> cls){
        if (!(this.getClass().equals(cls))) { // if this is not an instance of our Activity
            if (!(this instanceof HomeActivity)){  // check if this is not an instance of HomeActivity (Base)
                finish();                           // finish this activity, whatever it is (unless it's not a HomeActivity)
            }
            startActivity(cls);                     // start desired activity.
        }
    }
}

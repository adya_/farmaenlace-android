package com.xenio.adya.farmaenlace.ui.fragments;


import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.xenio.adya.farmaenlace.R;
import com.adya.tstools.navigation.TSFragment;

public class EditProfileFragment extends TSFragment {

    @Override
    protected void initializeFragmentView(View rootView, Bundle savedInstanceState) {
        Button bProfile = (Button) rootView.findViewById(R.id.bProfileSave);
        bProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTSActivity().closeCurrentFragment();
            }
        });
    }

    @Override
    protected int getLayoutResourceID() {
        return R.layout.fragment_edit_profile;
    }

    @Override
    public String getTitle() {
        return getString(R.string.text_header_profile);
    }
}

package com.xenio.adya.farmaenlace.ui.fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import com.adya.tstools.navigation.TSActivity;
import com.adya.tstools.navigation.TSFragment;
import com.xenio.adya.farmaenlace.R;
import com.xenio.adya.farmaenlace.models.Offer;
import com.xenio.adya.farmaenlace.ui.adapters.SearchAdapter;

public class SearchResultHomeFragment extends TSFragment {

    private Offer[] offers;
    private ListView lvSearchResults;
    private TextView tvResult;

    private SearchResultResetListener listener;

    @Override
    protected void initializeFragmentView(View rootView, Bundle savedInstanceState) {

        tvResult = (TextView) rootView.findViewById(R.id.tvSearchResultsNumber);
        TextView tvResetLink = (TextView) rootView.findViewById(R.id.tvResetSearchLink);

        lvSearchResults = (ListView) rootView.findViewById(R.id.lvSearchResults);
        if (offers != null) setResultItems(offers);
        lvSearchResults.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Bundle args = new Bundle();
                args.putSerializable(OfferFragment.ARGUMENT_KEY_OFFER, offers[i]);
                TSActivity.TSFragmentNavigationConfiguration configuration = getTSActivity().createNavigationConfiguration().setBundle(args);
                getTSActivity().addFragment(OfferFragment.class, configuration);
            }
        });


        tvResetLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) listener.onReset();
                ((TSFragment) getParentFragment()).getTSActivity().closeFragment(R.id.home_subcontainer, getFragmentManager());
            }
        });
    }

    public void setSearchResultResetListener(SearchResultResetListener listener) {
        this.listener = listener;
    }

    public void setResultItems(Offer[] items){
        offers = ((items == null) ? new Offer[0] : items);
        if (isVisible()) {
            lvSearchResults.setAdapter(new SearchAdapter(getParentFragment().getContext(), offers));
            if (offers.length == 0)
                tvResult.setText(R.string.text_label_search_results_not_found);
            else
                tvResult.setText(getString(R.string.pattern_label_search_results_number, offers.length));
        }
    }

    @Override
    protected int getLayoutResourceID() {
        return R.layout.fragment_home_search_result;
    }

    public interface SearchResultResetListener {
        void onReset();
    }
}

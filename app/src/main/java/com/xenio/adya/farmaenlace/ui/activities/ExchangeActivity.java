package com.xenio.adya.farmaenlace.ui.activities;

import android.support.v4.app.Fragment;
import com.xenio.adya.farmaenlace.ui.fragments.ExchangeFragment;

public class ExchangeActivity extends NavigationActivity {

    @Override
    public <T extends Fragment> Class<T> getInitialFragmentClass() {
        return (Class<T>) ExchangeFragment.class;
    }
}

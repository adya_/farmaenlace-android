package com.xenio.adya.farmaenlace.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ProgressBar;
import com.adya.tstools.navigation.TSActivity;
import com.adya.tstools.navigation.TSFragment;
import com.xenio.adya.farmaenlace.R;
import com.xenio.adya.farmaenlace.managers.MedicinesManager;
import com.xenio.adya.farmaenlace.models.Medicine;
import com.xenio.adya.farmaenlace.models.Offer;
import com.xenio.adya.farmaenlace.utils.MedicineUtils;

public class HomeFragment extends TSFragment {

    private TSActivity.TSFragmentNavigationConfiguration configuration;
    private AutoCompleteTextView etSearch;
    private Button bSearch;
    private ProgressBar pbSearchTerm;

    @Override
    protected void initializeFragmentView(View rootView, Bundle savedInstanceState) {
        configuration = getTSActivity().createNavigationConfiguration().setContainerID(R.id.home_subcontainer)
                                                                        .setFragmentManager(getChildFragmentManager());
        showMain();

        bSearch = (Button) rootView.findViewById(R.id.bSearch);
        etSearch = (AutoCompleteTextView) rootView.findViewById(R.id.etSearch);
        pbSearchTerm = (ProgressBar) rootView.findViewById(R.id.pbSearchTerm);

        setSearchProgressVisible(false);
        bSearch.setEnabled(false);

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void afterTextChanged(final Editable editable) {
                if (!TextUtils.isEmpty(editable)) {
                    setSearchProgressVisible(true);
                    MedicinesManager.getManager().performLoadSimilarMedicines(editable.toString(), new MedicinesManager.LoadingCallback<Medicine>() {
                        @Override
                        public void onLoaded(Medicine[] items) {
                            setSearchProgressVisible(false);
                            etSearch.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, MedicineUtils.getMedicinesNames(items)));
                            String seatchTerm = editable.toString();
                            bSearch.setEnabled(MedicineUtils.containsMedicine(items, seatchTerm));
                            if (!MedicineUtils.isUniqueMedicine(items, seatchTerm))
                                etSearch.showDropDown();
                        }

                        @Override
                        public void onFailure(MedicinesManager.MedicinesError error) {
                            setSearchProgressVisible(false);
                            etSearch.setAdapter(null);
                            bSearch.setEnabled(false);
                        }
                    });
                }
            }
        });

        bSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String term = etSearch.getText().toString();
                if (!term.isEmpty()) {
                    MedicinesManager.getManager().performSearchMedicines(term, new MedicinesManager.LoadingCallback<Offer>() {
                        @Override
                        public void onLoaded(Offer[] items) {
                            showSearchResults(items);
                        }

                        @Override
                        public void onFailure(MedicinesManager.MedicinesError error) {
                            if (error == MedicinesManager.MedicinesError.NOT_FOUND)
                               showSearchResults(null);
                        }
                    });
                }
            }
        });
    }

    private void setSearchProgressVisible(boolean isVisible){
        pbSearchTerm.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    private void showMain(){
        getTSActivity().replaceFragment(MainHomeFragment.class, configuration);
    }

    private void showSearchResults(Offer[] items) {
        if (getChildFragmentManager().getFragments().size() > 0){
            Fragment fragment = getChildFragmentManager().getFragments().get(0);
            if (!(fragment instanceof SearchResultHomeFragment)) {
                fragment = getTSActivity().addFragment(SearchResultHomeFragment.class, configuration);
            }
            ((SearchResultHomeFragment) fragment).setSearchResultResetListener(new SearchResultHomeFragment.SearchResultResetListener() {
                @Override
                public void onReset() {
                    etSearch.setText("");
                }
            });
            ((SearchResultHomeFragment) fragment).setResultItems(items);
        }
    }

    @Override
    public String getTitle() {
        return getString(R.string.text_header_home);
    }

    @Override
    protected int getLayoutResourceID() {
        return R.layout.fragment_home;
    }


}

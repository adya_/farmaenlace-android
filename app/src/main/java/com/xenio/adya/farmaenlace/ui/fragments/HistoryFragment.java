package com.xenio.adya.farmaenlace.ui.fragments;


import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import com.xenio.adya.farmaenlace.R;
import com.xenio.adya.farmaenlace.utils.Utils;
import com.adya.tstools.navigation.TSFragment;
import com.xenio.adya.farmaenlace.models.Offer;
import com.xenio.adya.farmaenlace.ui.adapters.HistoryAdapter;

public class HistoryFragment extends TSFragment {

    @Override
    protected void initializeFragmentView(View rootView, Bundle savedInstanceState) {
        ListView lvHistory = (ListView) rootView.findViewById(R.id.lvHistory);

        Offer[] offers = Utils.generateTestHistoryOffers();

        lvHistory.setAdapter(new HistoryAdapter(getContext(), offers));
    }

    @Override
    protected int getLayoutResourceID() {
        return R.layout.fragment_history;
    }

    @Override
    public String getTitle() {
        return getString(R.string.text_header_history);
    }
}

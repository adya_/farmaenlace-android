package com.xenio.adya.farmaenlace.models;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

public abstract class Offer implements Serializable{

    private OfferType type;
    private Medicine[] offeredMedicines;
    private Contact contact;
    private Date completionDate;

    protected Offer(OfferType type, Collection<Medicine> offeredMedicines, Contact contact) {
        this.type = type;
        this.offeredMedicines = offeredMedicines.toArray(new Medicine[0]);
        this.contact = contact;
    }

    protected Offer(OfferType type, Medicine[] offeredMedicines, Contact contact) {
        this.type = type;
        this.offeredMedicines = offeredMedicines;
        this.contact = contact;
    }

    public Contact getContact() {
        return contact;
    }

    public OfferType getType() {
        return type;
    }

    public boolean isCompleted(){
        return completionDate != null;
    }

    public Date getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(Date completionDate) {
        this.completionDate = completionDate;
    }

    public Medicine[] getOfferedMedicines() {
        return offeredMedicines;
    }





}

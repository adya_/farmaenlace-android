package com.xenio.adya.farmaenlace.netowrking;

import com.xenio.adya.farmaenlace.models.Medicine;
import com.xenio.adya.farmaenlace.models.Offer;
import com.xenio.adya.farmaenlace.models.User;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface FarmaenlaceService  {

    String BaseURL = "http://xenioit.com/xenio/backend/farma_enlace/dashboard/api.php/";

    @GET("?TYPE=LOGIN")
    Call<FEListResponse<User>> login(@Query("USER_NAME") String username,
                                     @Query("USER_PASSWORD") String password);

    @GET("?TYPE=LOGOUT")
    Call<FEEmptyResponse> logout(@Query("TOKEN") String token);

    @GET("?TYPE=GET_MEDICINES_BY_NAME")
    Call<FEListResponse<Offer>> getMedicines(@Query("NAME") String medicineName,
                                             @Query("TOKEN") String token);

    @GET("?TYPE=GET_SIMILAR_MEDICINES_BY_NAME")
    Call<FEListResponse<Medicine>> getSimilarMedicines(@Query("NAME") String medicineName,
                                     @Query("TOKEN") String token);

    @GET("?TYPE=LOCATE_MEDICINES")
    Call<FEEmptyResponse> addMedicineLocation(@Query("MEDICINE_NAME") String medicineName,
                                     @Query("PLACE_NAME") String locationName,
                                     @Query("PLACE_ADDRESS") String locationAddress,
                                     @Query("PLACE_LOC") String locationCoords,
                                     @Query("TOKEN") String token);

    @GET("?TYPE=UNLOCATE_MEDICINES")
    Call<FEEmptyResponse> removeMedicineLocation(@Query("MEDICINE_ID") int id,
                                        @Query("TOKEN") String token);

    @GET("?TYPE=CHANGE_MEDICINES")
    Call<FEEmptyResponse> addMedicinesExchange(@Query("MEDICINE_NAME") String medicineName,
                                      @Query("DETAILS") String medicineDetails,
                                      @Query("TOKEN") String token);

    @GET("?TYPE=DELETE_CHANGE_MEDICINES")
    Call<FEEmptyResponse> removeMedicinesExchange(@Query("MEDICINE_ID") int id,
                                         @Query("TOKEN") String token);

    @GET("?TYPE=DONATE_MEDICINES")
    Call<FEEmptyResponse> addMedicinesDonation(@Query("MEDICINE_NAME") String medicineName,
                                      @Query("DETAILS") String medicineDetails,
                                      @Query("TOKEN") String token);

    @GET("?TYPE=DELETE_DONATE_MEDICINES")
    Call<FEEmptyResponse> removeMedicinesDonation(@Query("MEDICINE_ID") int id,
                                         @Query("TOKEN") String token);

}

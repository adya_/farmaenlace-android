package com.xenio.adya.farmaenlace.models;

import com.google.android.gms.maps.model.LatLng;

import java.util.Collection;

public class LocationOffer extends Offer{

    private LatLng location;

    public LocationOffer(Collection<Medicine> offeredMedicines, LatLng location, Contact contact) {
        super(OfferType.Locate, offeredMedicines, contact);
        this.location = location;
    }

    public LocationOffer(Medicine[] offeredMedicines, LatLng location, Contact contact) {
        super(OfferType.Locate, offeredMedicines, contact);
        this.location = location;
    }

    public LatLng getLocation() {
        return location;
    }
}

package com.xenio.adya.farmaenlace.models;

import com.google.gson.annotations.SerializedName;

public enum OfferType {

    @SerializedName("CHANGE")
    Exchange,

    @SerializedName("DONATE")
    Donate,

    @SerializedName("LOCATE")
    Locate
}

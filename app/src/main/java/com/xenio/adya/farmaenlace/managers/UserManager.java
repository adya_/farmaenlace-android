package com.xenio.adya.farmaenlace.managers;

import android.text.TextUtils;
import com.adya.tstools.networking.TSResponse;
import com.adya.tstools.networking.TSRetrofitManager;
import com.xenio.adya.farmaenlace.models.User;
import com.xenio.adya.farmaenlace.netowrking.FarmaenlaceManager;

import java.util.List;

public class UserManager {

    private static UserManager manager;
    private UserManager() {}
    public static UserManager getManager() {
        if (manager == null) manager = new UserManager();
        return manager;
    }

    private User authorizedUser;

    public User getAuthorizedUser() {
        return authorizedUser;
    }
    public boolean isAuthorized(){ return authorizedUser != null && !TextUtils.isEmpty(authorizedUser.getToken());}

    public void performLogin(String username, String password, final LoginCallback callback){
        FarmaenlaceManager.getManager().login(username, password, new TSRetrofitManager.TSResponseCallback<List<User>>() {
            @Override
            public void onSuccess(TSResponse<List<User>> response) {
                authorizedUser = response.getBody().get(0);
                if (callback != null) callback.onLogin(authorizedUser);
            }

            @Override
            public void onFailure() {
                if (callback != null) callback.onFailure(LoginError.UNKNOWN_ERROR);
            }
        });
    }

    public void performLogout( final LogoutCallback callback){
        FarmaenlaceManager.getManager().logout(authorizedUser.getToken(), new TSRetrofitManager.TSResponseCallback() {
            @Override
            public void onSuccess(TSResponse response) {
                authorizedUser = null;
                if (callback != null) callback.onLogout();
            }

            @Override
            public void onFailure() {
                if (callback != null) callback.onFailure(LoginError.UNKNOWN_ERROR);
            }
        });
    }

    public void performRegistration(String username, String password, final RegistrationCallback callback){

    }


    public interface LoginCallback {
        void onLogin(User user);
        void onFailure(LoginError error);
    }

    public interface LogoutCallback {
        void onLogout();
        void onFailure(LoginError error);
    }

    public interface RegistrationCallback {
        void onRegister(User user);
        void onFailure(LoginError error);
    }

    public enum LoginError {
        UNKNOWN_ERROR,
        INVALID_USERNAME,
        INVALID_PASSWORD
    }
}

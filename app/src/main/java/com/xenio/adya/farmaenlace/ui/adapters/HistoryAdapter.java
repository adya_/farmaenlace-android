package com.xenio.adya.farmaenlace.ui.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.xenio.adya.farmaenlace.R;
import com.xenio.adya.farmaenlace.models.ExchangeOffer;
import com.xenio.adya.farmaenlace.models.Offer;
import com.xenio.adya.farmaenlace.models.OfferType;
import com.xenio.adya.farmaenlace.utils.OfferUtils;

import java.util.ArrayList;

public class HistoryAdapter extends BaseAdapter {

    private Offer[] offers;
    private Context context;
    private LayoutInflater inflater;

    public HistoryAdapter(Context context, ArrayList<Offer> offers){
        this.offers = offers.toArray(new Offer[0]);
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    public HistoryAdapter(Context context, Offer[] offers){
        this.offers = offers;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return offers.length;
    }

    @Override
    public Offer getItem(int i) {
        return offers[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View recyledView, ViewGroup viewGroup) {
        View view = recyledView;
        if (view == null){
            view = inflater.inflate(R.layout.item_history, viewGroup, false);
        }
        TextView tvOffer = (TextView) view.findViewById(R.id.tvHistoryItemOfferType);
        TextView tvMedicines = (TextView) view.findViewById(R.id.tvHistoryItemMedicine);
        TextView tvDetails = (TextView) view.findViewById(R.id.tvHistoryItemDetails);
        TextView tvExchanged = (TextView) view.findViewById(R.id.tvHistoryItemExchanged);
        TextView tvDate = (TextView) view.findViewById(R.id.tvHistoryItemDate);

        Offer offer = getItem(i);

        tvDate.setVisibility(offer.isCompleted() ? View.VISIBLE : View.GONE);
        if (offer.isCompleted()) {
            String dateString = android.text.format.DateFormat.format("MM/dd/yyyy", offer.getCompletionDate()).toString();
            tvDate.setText(dateString);
        }
        tvOffer.setText(offerName(offer.getType()));
        tvMedicines.setText(TextUtils.join(", ", OfferUtils.getOfferedMedicinesNames(offer)));
        tvDetails.setText(TextUtils.join(", ",  OfferUtils.getOfferedMedicinesDetails(offer)));

        tvExchanged.setVisibility(offer.getType() == OfferType.Exchange ? View.VISIBLE : View.GONE);
        if (offer.getType() == OfferType.Exchange) {
            tvExchanged.setText(context.getString(R.string.pattern_label_history_item_exchanged, TextUtils.join(", ", OfferUtils.getRequestedMedicinesNames(((ExchangeOffer) offer)))));
        }
        return view;
    }

    private String offerName(OfferType type){
        switch (type){
            case Exchange:return context.getString(R.string.value_offer_name_exchange);
            case Donate: return context.getString(R.string.value_offer_name_donate);
            case Locate: return context.getString(R.string.value_offer_name_locate);
            default: return "";
        }
    }
}

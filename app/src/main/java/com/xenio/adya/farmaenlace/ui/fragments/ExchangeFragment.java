package com.xenio.adya.farmaenlace.ui.fragments;

import android.database.DataSetObserver;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.xenio.adya.farmaenlace.R;
import com.xenio.adya.farmaenlace.utils.Utils;
import com.adya.tstools.navigation.TSFragment;
import com.xenio.adya.farmaenlace.models.Medicine;
import com.xenio.adya.farmaenlace.models.Offer;
import com.xenio.adya.farmaenlace.ui.adapters.MedicineAdapter;
import com.xenio.adya.farmaenlace.ui.adapters.OfferAdapter;

import java.util.Random;

public class ExchangeFragment extends TSFragment {

    private static final int MAX_NEEDS = 3;
    private static final int MAX_OFFERS = 5;

    private Medicine[] needMedicines;
    private Offer[] haveOffers;

    private OfferAdapter offerAdapter;
    private MedicineAdapter medicineAdapter;

    private ListView lvNeeds;
    private ListView lvOffers;

    @Override
    protected int getLayoutResourceID() {
        return R.layout.fragment_exchange;
    }

    @Override
    public String getTitle() {
        return getString(R.string.text_header_exchange);
    }

    @Override
    protected void initializeFragmentView(View rootView, Bundle savedInstanceState) {

        haveOffers = Utils.generateTestOffers();
        needMedicines = haveOffers[2].getOfferedMedicines();

        medicineAdapter = new MedicineAdapter(getContext(), new Medicine[0]);
        offerAdapter = new OfferAdapter(getContext(), new Offer[0]);

        medicineAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                setListViewHeightBasedOnItems(lvNeeds);
            }
        });
        offerAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                setListViewHeightBasedOnItems(lvOffers);
            }
        });

        lvOffers = (ListView) rootView.findViewById(R.id.lvOffers);
        lvOffers.setAdapter(offerAdapter);

        lvNeeds = (ListView) rootView.findViewById(R.id.lvNeeds);
        lvNeeds.setAdapter(medicineAdapter);

        Button bPublish = (Button) rootView.findViewById(R.id.bExchangePublish);
        Button bAddNeed = (Button) rootView.findViewById(R.id.bNeedAdd);
        Button bAddHave = (Button) rootView.findViewById(R.id.bOfferAdd);

        bPublish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTSActivity().finish();
            }
        });
        bAddNeed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (medicineAdapter.getCount() < MAX_NEEDS) {
                    medicineAdapter.add(needMedicines[new Random(System.currentTimeMillis()).nextInt(needMedicines.length)]);
                    setListViewHeightBasedOnItems(lvNeeds);
                }
            }
        });
        bAddHave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (offerAdapter.getCount() < MAX_OFFERS) {
                    offerAdapter.add(haveOffers[new Random().nextInt(haveOffers.length)]);
                    setListViewHeightBasedOnItems(lvOffers);
                }
            }
        });



    }

    /**
     * Sets ListView height dynamically based on the height of the items.
     *
     * @param listView to be resized
     * @return true if the listView is successfully resized, false otherwise
     */
    private static boolean setListViewHeightBasedOnItems(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                item.measure(View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.EXACTLY),
                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();

            return true;

        } else {
            return false;
        }

    }
}

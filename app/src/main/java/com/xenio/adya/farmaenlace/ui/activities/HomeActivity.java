package com.xenio.adya.farmaenlace.ui.activities;

import android.support.v4.app.Fragment;
import com.xenio.adya.farmaenlace.ui.fragments.HomeFragment;

public class HomeActivity extends NavigationActivity {

    @Override
    public <T extends Fragment> Class<T> getInitialFragmentClass() {
        return (Class<T>) HomeFragment.class;
    }


}

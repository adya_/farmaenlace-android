package com.xenio.adya.farmaenlace.utils;

import android.text.TextUtils;
import com.xenio.adya.farmaenlace.models.ExchangeOffer;
import com.xenio.adya.farmaenlace.models.Offer;

public class OfferUtils {
    private OfferUtils(){}


    public static String[] getOfferedMedicinesNames(Offer offer){
        return MedicineUtils.getMedicinesNames(offer.getOfferedMedicines());
    }

    public static String[] getOfferedMedicinesDetails(Offer offer){
        return MedicineUtils.getMedicinesDetails(offer.getOfferedMedicines());
    }

    public static String[] getRequestedMedicinesNames(ExchangeOffer offer){
        return MedicineUtils.getMedicinesNames(offer.getRequestedMedicines());
    }

    public static String[] getRequestedMedicinesDetails(ExchangeOffer offer){
        return MedicineUtils.getMedicinesDetails(offer.getRequestedMedicines());
    }

    private static String joinStringArray(String[] array){
        if (array != null)
            return TextUtils.join(", ", array);
        else return "";
    }
}

package com.xenio.adya.farmaenlace.ui.fragments;

import android.database.DataSetObserver;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.adya.tstools.navigation.TSFragment;
import com.adya.tstools.ui.TSNotifier;
import com.xenio.adya.farmaenlace.R;
import com.xenio.adya.farmaenlace.managers.MedicinesManager;
import com.xenio.adya.farmaenlace.models.DonationOffer;
import com.xenio.adya.farmaenlace.models.Medicine;
import com.xenio.adya.farmaenlace.models.Offer;
import com.xenio.adya.farmaenlace.ui.adapters.OfferAdapter;
import com.xenio.adya.farmaenlace.utils.MedicineUtils;
import com.xenio.adya.farmaenlace.utils.Utils;

public class DonateFragment extends TSFragment {

    private static final int MAX_OFFERS = 5;

    private Offer[] haveOffers;

    private String medicineName;
    private String medicineDetails;
    private boolean isValidMedicineName;

    private OfferAdapter offerAdapter;
    private ListView lvDonate;

    private AutoCompleteTextView etOffer;
    private EditText etDetails;
    private ProgressBar pbSearchTerm;
    private Button bAddOffer;
    private Button bPublish;

    @Override
    protected int getLayoutResourceID() {
        return R.layout.fragment_donate;
    }

    @Override
    public String getTitle() {
        return getString(R.string.text_header_donate);
    }

    @Override
    protected void initializeFragmentView(View rootView, Bundle savedInstanceState) {

        haveOffers = Utils.generateTestOffers();
        offerAdapter = new OfferAdapter(getContext(), new Offer[0]);
        offerAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                setListViewHeightBasedOnItems(lvDonate);
                validateOffer();
            }
        });
        etOffer = (AutoCompleteTextView) rootView.findViewById(R.id.etOffer);
        etDetails = (EditText) rootView.findViewById(R.id.etOfferDetails);
        lvDonate = (ListView) rootView.findViewById(R.id.lvDonate);
        lvDonate.setAdapter(offerAdapter);
        pbSearchTerm = (ProgressBar) rootView.findViewById(R.id.pbSearchTerm);

        bPublish = (Button) rootView.findViewById(R.id.bDonatePublish);
        bAddOffer = (Button) rootView.findViewById(R.id.bDonateAdd);

        bPublish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TSNotifier.getNotifier().showProgress();
                MedicinesManager.getManager().performAddOffer(offerAdapter.getItem(0), new MedicinesManager.OffersCallback() {
                    @Override
                    public void onSuccess() {
                        TSNotifier.getNotifier().hideProgress();
                        getTSActivity().finish();
                    }

                    @Override
                    public void onFailure(MedicinesManager.MedicinesError error) {
                        TSNotifier.getNotifier().hideProgress();
                    }
                });

            }
        });

        bAddOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (canAddMedicines()) {
                    offerAdapter.add(new DonationOffer(new Medicine[]{new Medicine(medicineName, medicineDetails)}, Utils.generateTestContact()));
                    setListViewHeightBasedOnItems(lvDonate);
                }
                validateOffer();
            }
        });

        etOffer.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void afterTextChanged(final Editable editable) {
                medicineName = editable.toString();
                if (!TextUtils.isEmpty(medicineName)) {
                    setSearchProgressVisible(true);
                    MedicinesManager.getManager().performLoadSimilarMedicines(medicineName, new MedicinesManager.LoadingCallback<Medicine>() {
                        @Override
                        public void onLoaded(Medicine[] items) {
                            setSearchProgressVisible(false);
                            etOffer.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, MedicineUtils.getMedicinesNames(items)));
                            isValidMedicineName = MedicineUtils.containsMedicine(items, medicineName);
                            validateMedicine();
                            if (!MedicineUtils.isUniqueMedicine(items, medicineName))
                                etOffer.showDropDown();
                        }

                        @Override
                        public void onFailure(MedicinesManager.MedicinesError error) {
                            setSearchProgressVisible(false);
                            etOffer.setAdapter(null);
                            isValidMedicineName = false;
                            validateMedicine();
                        }
                    });
                }
            }
        });

        etDetails.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void afterTextChanged(Editable editable) { medicineDetails = editable.toString(); validateMedicine(); }
        });
        validateMedicine();
        validateOffer();
    }

    private boolean canAddMedicines(){
        return (offerAdapter != null) && (offerAdapter.getCount() < MAX_OFFERS);
    }

    private void validateMedicine(){
        boolean isValid = isValidMedicineName && !TextUtils.isEmpty(medicineDetails);
        bAddOffer.setEnabled(isValid && canAddMedicines());
    }

    private void validateOffer(){
        bPublish.setEnabled(offerAdapter.getCount() > 0);
    }

    private void setSearchProgressVisible(boolean isVisible){
        pbSearchTerm.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    ///TODO: Create AutosizeListView
    /**
     * Sets ListView height dynamically based on the height of the items.
     *
     * @param listView to be resized
     * @return true if the listView is successfully resized, false otherwise
     */
    private static boolean setListViewHeightBasedOnItems(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                item.measure(View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.EXACTLY),
                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();

            return true;

        } else {
            return false;
        }
    }
}

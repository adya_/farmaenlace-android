package com.xenio.adya.farmaenlace.ui.fragments;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.xenio.adya.farmaenlace.R;
import com.adya.tstools.navigation.TSFragment;
import com.xenio.adya.farmaenlace.models.LocationOffer;
import com.xenio.adya.farmaenlace.utils.OfferUtils;


public class LocateMapFragment extends TSFragment implements OnMapReadyCallback, LocationListener {

    public static final String ARGUMENT_KEY_EDITABLE = "EditableMap";
    public static final String ARGUMENT_KEY_OFFER = "Offer";

    private boolean isEditable;
    private LocationOffer offer;

    private GoogleMap mMap;
    private LocationManager locationManager;

    @Override
    protected int getLayoutResourceID() {
        return R.layout.fragment_locate_map;
    }

    @Override
    public String getTitle() {
        return getString(R.string.text_header_locate);
    }


    @Override
    protected void initializeFragmentView(View rootView, Bundle savedInstanceState) {
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        isEditable = getArguments().getBoolean(ARGUMENT_KEY_EDITABLE);
        if (!isEditable)
            offer = (LocationOffer) getArguments().getSerializable(ARGUMENT_KEY_OFFER);

        Button bSave = (Button) rootView.findViewById(R.id.bMapSave);
        Button bBack = (Button) rootView.findViewById(R.id.bMapBack);

        bSave.setVisibility(isEditable ? View.VISIBLE : View.GONE);

        bSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTSActivity().closeCurrentFragment();
            }
        });

        bBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTSActivity().closeCurrentFragment();
            }
        });
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (!isEditable) {
            mMap.addMarker(new MarkerOptions().position(offer.getLocation()).title(TextUtils.join(", ", OfferUtils.getOfferedMedicinesNames(offer))));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(offer.getLocation()));
        } else {
            locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5 * 60 * 1000, 1000, this);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(latLng);
        mMap.moveCamera(cameraUpdate);
        locationManager.removeUpdates(this);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}

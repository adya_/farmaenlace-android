package com.xenio.adya.farmaenlace.models;

import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName("name")
    private String name;

    @SerializedName("TOKEN")
    private String token;

    public String getName() {
        return name;
    }

    public String getToken() {
        return token;
    }
}

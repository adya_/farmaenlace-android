package com.xenio.adya.farmaenlace.netowrking.converters;

import com.google.gson.*;
import com.xenio.adya.farmaenlace.models.*;
import com.xenio.adya.farmaenlace.utils.Utils;

import java.lang.reflect.Type;

public class OfferJSONDeserializer implements JsonDeserializer<Offer> {
    @Override
    public Offer deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jOffer = (JsonObject) json;


        int id = jOffer.get("id").getAsInt();
        OfferType type = context.deserialize(jOffer.get("type"), OfferType.class);
        String medName = jOffer.get("name").getAsString();
        String medDetails = jOffer.get("details").getAsString();

        Medicine[] meds = new Medicine[]{new Medicine(medName, medDetails)};
        switch (type) {
            case Exchange: return new ExchangeOffer(meds, new Medicine[0], Utils.generateTestContact());
            case Donate: return new DonationOffer(meds, Utils.generateTestContact());
            case Locate: return new LocationOffer(meds, Utils.generateTestLocation(), Utils.generateTestContact());
        }
        return null;
    }
}

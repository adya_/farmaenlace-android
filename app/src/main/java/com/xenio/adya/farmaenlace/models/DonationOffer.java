package com.xenio.adya.farmaenlace.models;

import java.util.Collection;

public class DonationOffer extends Offer {

    public DonationOffer(Collection<Medicine> offeredMedicines, Contact contact) {
        super(OfferType.Donate, offeredMedicines, contact);
    }

    public DonationOffer(Medicine[] offeredMedicines, Contact contact) {
        super(OfferType.Donate, offeredMedicines, contact);
    }
}

package com.xenio.adya.farmaenlace.ui.fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.adya.tstools.navigation.TSFragment;
import com.xenio.adya.farmaenlace.R;
import com.xenio.adya.farmaenlace.ui.activities.HomeActivity;

public class RegisterFragment extends TSFragment {

    @Override
    protected void initializeFragmentView(View rootView, Bundle savedInstanceState) {
        Button bRegister = (Button) rootView.findViewById(R.id.bRegister);

        bRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTSActivity().finish();
                getTSActivity().startActivity(HomeActivity.class);
            }
        });
    }

    @Override
    protected int getLayoutResourceID() {
        return R.layout.fragment_register;
    }
}

package com.xenio.adya.farmaenlace.models;

import java.io.Serializable;

public class Medicine implements Serializable {

    private int id;

    private String name;

    private String details;

    public Medicine(String name, String details) {
        this.name = name;
        this.details = details;
    }

    public String getName() {
        return name;
    }

    public String getDetails() {
        return details;
    }
}

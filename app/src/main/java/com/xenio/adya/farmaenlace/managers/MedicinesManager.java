package com.xenio.adya.farmaenlace.managers;

import com.adya.tstools.networking.TSResponse;
import com.adya.tstools.networking.TSRetrofitManager;
import com.xenio.adya.farmaenlace.models.*;
import com.xenio.adya.farmaenlace.netowrking.FarmaenlaceManager;

import java.util.List;

public class MedicinesManager {
    private static MedicinesManager manager;
    private MedicinesManager() {}
    public static MedicinesManager getManager() {
        if (manager == null) manager = new MedicinesManager();
        return manager;
    }

    public void performLoadSimilarMedicines(String term, final LoadingCallback<Medicine> callback){
        if (UserManager.getManager().isAuthorized()) {
            FarmaenlaceManager.getManager().getSimilarMedicines(term, UserManager.getManager().getAuthorizedUser().getToken(), new TSRetrofitManager.TSResponseCallback<List<Medicine>>() {
                @Override
                public void onSuccess(TSResponse<List<Medicine>> response) {
                    if (callback == null) return;
                    if (response.getBody().size() > 0) {
                        callback.onLoaded(response.getBody().toArray(new Medicine[0]));
                    }
                    else
                        callback.onFailure(MedicinesError.NOT_FOUND);
                }

                @Override
                public void onFailure() {
                    if (callback != null) callback.onFailure(MedicinesError.UNKNOWN_ERROR);
                }
            });
        }
        else{
            if (callback != null) callback.onFailure(MedicinesError.NOT_AUTHORIZED);
        }
    }

    public void performSearchMedicines(String term, final LoadingCallback<Offer> callback){
        if (UserManager.getManager().isAuthorized()) {
            FarmaenlaceManager.getManager().getMedicines(term, UserManager.getManager().getAuthorizedUser().getToken(), new TSRetrofitManager.TSResponseCallback<List<Offer>>() {
                @Override
                public void onSuccess(TSResponse<List<Offer>> response) {
                    if (callback == null) return;
                    if (response.getBody().size() > 0) {
                        callback.onLoaded(response.getBody().toArray(new Offer[0]));
                    }
                    else
                        callback.onFailure(MedicinesError.NOT_FOUND);
                }

                @Override
                public void onFailure() {
                    if (callback != null) callback.onFailure(MedicinesError.UNKNOWN_ERROR);
                }
            });
        }
        else{
            if (callback != null) callback.onFailure(MedicinesError.NOT_AUTHORIZED);
        }
    }

    public void performAddOffer(Offer offer, final OffersCallback callback){
        if (UserManager.getManager().isAuthorized()){
            switch (offer.getType()) {
                case Exchange:  performAddExchangeOffer(((ExchangeOffer) offer), callback);
                    break;
                case Donate: performAddDonationOffer(((DonationOffer) offer), callback);
                    break;
                case Locate: performAddLocationOffer(((LocationOffer) offer), callback);
                    break;
            }
        }
        else{
            if (callback != null) callback.onFailure(MedicinesError.NOT_AUTHORIZED);
        }
    }

    private void performAddLocationOffer(LocationOffer offer, final OffersCallback callback){
        FarmaenlaceManager.getManager().addMedicineLocation(offer.getOfferedMedicines()[0].getName(),
                                                            offer.getContact().getFullname(),
                                                            offer.getContact().getAddress(),
                                                            String.format("%1$f, %2$f", offer.getLocation().latitude, offer.getLocation().longitude),
                                                            UserManager.getManager().getAuthorizedUser().getToken(),
                                                            new TSRetrofitManager.TSResponseCallback() {
            @Override
            public void onSuccess(TSResponse response) {
                if (callback != null) callback.onSuccess();
            }

            @Override
            public void onFailure() {
                if (callback != null) callback.onFailure(MedicinesError.UNKNOWN_ERROR);
            }
        });
    }

    private void performAddDonationOffer(DonationOffer offer, final OffersCallback callback){
        FarmaenlaceManager.getManager().addMedicinesDonation(offer.getOfferedMedicines()[0].getName(),
                offer.getOfferedMedicines()[0].getDetails(),
                UserManager.getManager().getAuthorizedUser().getToken(),
                new TSRetrofitManager.TSResponseCallback() {
                    @Override
                    public void onSuccess(TSResponse response) {
                        if (callback != null) callback.onSuccess();
                    }

                    @Override
                    public void onFailure() {
                        if (callback != null) callback.onFailure(MedicinesError.UNKNOWN_ERROR);
                    }
                });
    }

    private void performAddExchangeOffer(ExchangeOffer offer, final OffersCallback callback){
        FarmaenlaceManager.getManager().addMedicinesExchange(offer.getOfferedMedicines()[0].getName(),
                offer.getOfferedMedicines()[0].getDetails(),
                UserManager.getManager().getAuthorizedUser().getToken(),
                new TSRetrofitManager.TSResponseCallback() {
                    @Override
                    public void onSuccess(TSResponse response) {
                        if (callback != null) callback.onSuccess();
                    }

                    @Override
                    public void onFailure() {
                        if (callback != null) callback.onFailure(MedicinesError.UNKNOWN_ERROR);
                    }
                });
    }

    public interface LoadingCallback<T> {
        void onLoaded(T[] items);
        void onFailure(MedicinesError error);
    }

    public interface OffersCallback {
        void onSuccess();
        void onFailure(MedicinesError error);
    }

    public enum MedicinesError{
        UNKNOWN_ERROR,
        NOT_FOUND,
        NOT_AUTHORIZED
    }
}

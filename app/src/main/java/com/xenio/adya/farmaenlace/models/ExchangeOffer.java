package com.xenio.adya.farmaenlace.models;

import java.util.Collection;

public class ExchangeOffer extends Offer {

    private Medicine[] requestedMedicines;

    public ExchangeOffer(Collection<Medicine> offeredMedicines, Collection<Medicine> requestedMedicines, Contact contact) {
        super(OfferType.Exchange, offeredMedicines, contact);
        this.requestedMedicines = requestedMedicines.toArray(new Medicine[0]);
    }

    public ExchangeOffer(Medicine[] offeredMedicines, Medicine[] requestedMedicines, Contact contact) {
        super(OfferType.Exchange, offeredMedicines, contact);
        this.requestedMedicines = requestedMedicines;
    }

    public Medicine[] getRequestedMedicines() {
        return requestedMedicines;
    }

}

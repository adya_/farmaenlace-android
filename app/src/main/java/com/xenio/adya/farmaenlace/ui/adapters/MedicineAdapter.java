package com.xenio.adya.farmaenlace.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import com.xenio.adya.farmaenlace.R;
import com.xenio.adya.farmaenlace.models.Medicine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class MedicineAdapter extends BaseAdapter{

    private List<Medicine> medicines;
    private Context context;
    private LayoutInflater inflater;

    public MedicineAdapter(Context context, Collection<Medicine> medicines){
        this.medicines = new ArrayList<>(medicines);
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    public MedicineAdapter(Context context, Medicine[] medicines){
        this.medicines = new ArrayList<>(Arrays.asList(medicines));
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return medicines.size();
    }

    @Override
    public Medicine getItem(int i) {
        return medicines.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View recyledView, ViewGroup viewGroup) {
        View view = recyledView;
        if (view == null){
            view = inflater.inflate(R.layout.item_want, viewGroup, false);
        }
        TextView tvMedicines = (TextView) view.findViewById(R.id.tvWantItemMedicine);
        final ImageButton ibRemove = (ImageButton) view.findViewById(R.id.ibWantItemRemove);
        final Medicine medicine = getItem(i);

        tvMedicines.setText(medicine.getName());

        ibRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                medicines.remove(i);
                notifyDataSetChanged();
            }
        });

        return view;
    }

    public void add(Medicine medicine){
        medicines.add(medicine);
        notifyDataSetChanged();
    }
}
